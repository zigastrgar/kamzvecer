<?php
require __DIR__ . "/bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

if ( $_GET['method'] == "up" ) {
    Capsule::schema()->dropIfExists('events');
    Capsule::schema()->dropIfExists('organizers');
    Capsule::schema()->dropIfExists('categories');
    Capsule::schema()->dropIfExists('kategorije');
    Capsule::schema()->dropIfExists('izvajalci');
    Capsule::schema()->create('events', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('name_raw');
        $table->string('location')->nullable();
        $table->text('description');
        $table->text('description_raw');
        $table->string("price")->nullable();
        $table->string('link')->nullable();
        $table->integer('organizer_id');
        $table->boolean('published')->default(false);
        $table->timestamps();
        $table->softDeletes();
        $table->datetime('start_time');
        $table->dateTime('end_time')->nullable();
    });

    Capsule::schema()->create('organizers', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->text('about');
        $table->string('address');
        $table->string('link')->nullable();
        $table->string('facebook_id')->nullable();
        $table->integer('category_id')->nullable();
        $table->timestamps();
    });

    Capsule::schema()->create('categories', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->timestamps();
    });

    Capsule::schema()->create('category_event', function(Blueprint $table) {
        $table->integer('event_id')->unsigned();
        $table->integer('category_id')->unsigned();
        $table->primary([ 'event_id', 'category_id' ]);
    });

    Capsule::schema()->create('kategorije', function(Blueprint $table) {
        $table->string('name');
        $table->string('code');
    });

    Capsule::schema()->create('izvajalci', function(Blueprint $table) {
        $table->string('id')->nullable();
        $table->string('name')->nullable();
        $table->string('facebook')->nullable();
        $table->string('code')->nullable();
        $table->string('options')->nullable();
        $table->string('additional')->nullable();
    });

} else if ( $_GET['method'] == "down" ) {
    if ( $_GET['n'] == 1 ) {
        Capsule::schema()->drop('events');
    } else if ( $_GET['n'] == 2 ) {
        Capsule::schema()->drop('categories');
    } else if ( $_GET['n'] == 3 ) {
        Capsule::schema()->drop('organizers');
    } else {
        Capsule::schema()->drop('events');
        Capsule::schema()->drop('organizers');
        Capsule::schema()->drop('categories');
    }
} else if ( $_GET["method"] == "truncate" ) {
    Capsule::table('events')->truncate();
}
