<?php
require __DIR__ . "/bootstrap.php";
$id = (int)$_GET['id'];
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vsi dogodki</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <h2>Urejanje dogodka</h2>
        <?php
        $event = Event::findOrFail($id);
        $categories = Category::all();
        ?>
        <form method="POST" action="editing_event.php?id=<?= $id ?>">
            <div class='form-group'>
                <label for="name">Ime dogodka</label>
                <input type="text" name="name" value="<?= $event->name ?>" class="form-control">
            </div>
            <div class='form-group'>
                <label for="price">Vstopnina</label>
                <input type="number" name="price" value="<?= $event->price ?>" class="form-control">
            </div>
            <div class='form-group'>
                <label for="description">Opis dogodka</label>
                <textarea rows="10" name="description" class="form-control"><?= $event->description ?></textarea>
            </div>
            <div class="form-group">
                <label for="categories">Zvrst</label>
                <select name="categories[]" multiple id="categories" class="form-control">
                    <?php foreach($categories as $category): ?>
                        <option value="<?= $category->id ?>" <?= (in_array($category->id, $event->categories->pluck('id'))) ? "selected" : "" ?>><?= $category->name ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class='form-group'>
                <label for="link">Povezava do dogodka</label>
                <input type="text" name="link" value="<?= $event->link ?>" class="form-control">
            </div>
            <div class='form-group'>
                <label for="link">Začetek dogodka</label>
                <input type="datetime" name="start_time" value="<?= $event->start_time ?>" class="form-control">
            </div>
            <div class='form-group'>
                <label for="link">Konec dogodka</label>
                <input type="datetime" name="end_time" value="<?= $event->end_time ?>" class="form-control">
            </div>
            <div class='form-group'>
                <label for="link">Objavljen</label>
                <input type="checkbox" name="published" <?= ( $event->published == 1 ) ? "checked" : "" ?>>
            </div>
            <div class='form-group'>
                <input type="submit" class="btn btn-primary" value="Uredi dogodek">
            </div>
        </form>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
</body>
</html>
