<?php
require __DIR__ . "/bootstrap.php";

$link = $_POST["link"];
$value = $_POST['category'];

function getFacebookUsername($link)
{
    $www = "";
    if ( strpos($link, "//www.") !== false ) {
        $www = "www.";
    }
    $remaining = str_replace("https://{$www}facebook.com/", "", $link);

    return explode("/", $remaining)[0];
}

$username = getFacebookUsername($link);

try {
    $response = $fb->get('/' . $username . '?fields=id,name,about,location,link', $_SESSION['fb_access_token']);
} catch ( Facebook\Exceptions\FacebookResponseException $e ) {
    echo 'Graph2 returned an error: ' . $e->getMessage();
    exit;
} catch ( Facebook\Exceptions\FacebookSDKException $e ) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

$user = $response->getGraphUser();

Organizer::create([
    'name'        => $user['name'],
    'about'       => $user['about'],
    'link'        => $user['link'],
    'address'     => $user['location']['street'] . ", " . $user['location']['zip'] . " " . $user['location']['city'],
    'facebook_id' => $user['id'],
    'category_id' => $value
]);

header("Location: ./add_organizer.php");