<?php
require __DIR__ . "/bootstrap.php";

$helper = $fb->getRedirectLoginHelper();

$permissions = [ 'email' ];
$loginUrl    = $helper->getLoginUrl('http://localhost:8000/callback.php', $permissions);

echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
