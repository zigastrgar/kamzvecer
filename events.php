<?php
require __DIR__ . "/bootstrap.php";
$categories = Category::all();
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vsi dogodki</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/select2.min.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <h2>Verzija .01
            <small><span class="label label-warning">BETA</span></small>
        </h2>
        <p>Zenkrat se podatke ne obdelujejo, kot dobimo iz Facebooka tako se prikaže tu</p>
        <p><strong>Opis je zaenkrt skrit zaradi količine teksta!</strong></p>
        <a class="btn btn-success" href="add_category.php">Dodaj kategorijo</a>
        <a class="btn btn-success" href="add_organizer.php">Dodaj organizatorja</a>
        <p>Updejtanje eventov iz FB še ne deluje pravilno, bom popravil :) Urejanje na strani pa deluje. Dodal bom še
            vtičnik za lažje urejanje opisa.</p>
        <table class="table table-bordered table-responsive">
            <tr>
                <th>Ime dogodka</th>
                <th>Organizator</th>
                <th>Datum dogodka</th>
                <th>Ura dogodka</th>
                <th>Kategorije</th>
                <th>Vstopnina</th>
                <th>Akcija</th>
            </tr>
            <?php foreach ( Event::all() as $event ) { ?>
                <tr data-event="<?= $event->id ?>">
                    <td><?= $event->name ?></td>
                    <td><a href="<?= $event->organizer->link ?>" target="_blank"><?= $event->organizer->name ?></a></td>
                    <td><?= $event->start_time->format('d.m.Y') ?></td>
                    <td><?= $event->start_time->format('H:i') ?></td>
                    <td></td>
                    <td><?= ( is_null($event->price) ) ? "/" : $event->price ?></td>
                    <td>
                        <span style="cursor: pointer; display: inline-block;" class="btn btn-primary"
                              onclick="editEvent(<?= $event->id ?>);">Uredi</span>
                        <span style="cursor: pointer; display: inline-block;" class="btn btn-danger"
                              onclick="deleteEvent(<?= $event->id ?>);">Izbriši</span>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
<div class="modal fade large-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Urejanje dogodka</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Ime dogodka</label>
                    <input type="text" id="name" name="name" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="price">Vstopnina</label>
                    <input type="number" id="price" name="price" value="" class="form-control">
                    <span class="help-block">Če je polje prazno ni podatka o vstopnini</span>
                </div>
                <div class="form-group">
                    <label for="description">Opis dogodka</label>
                    <textarea rows="5" id="description" name="description" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="categories">Zvrsti</label>
                    <select name="categories[]" multiple="multiple" id="categories" class="form-control"
                            style="width: 100%">
                        <?php foreach ( $categories as $category ): ?>
                            <option value="<?= $category->id ?>"><?= $category->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class='form-group'>
                    <label for="link">Povezava do dogodka</label>
                    <input type="text" name="link" value="<?= $event->link ?>" class="form-control">
                </div>
                <div class='form-group'>
                    <label for="link">Začetek dogodka</label>
                    <input type="datetime" name="start_time" value="<?= $event->start_time ?>" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i>Shrani</button>
                <button type="button" class="btn btn-success">Shrani & Objavi</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Zapri</button>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
<script src="assets/select2.full.min.js"></script>
<script>
    var $select;

    function editEvent(id) {
        $.ajax({
            type: 'POST',
            url: 'get_event.php',
            data: {id: id},
            success: function (data) {
                formSetup(data);
                $(".large-modal").modal();
            }
        });
    }

    function formSetup(data) {
        if (!$select) {
            $select = setupSelect();
        }
        data = JSON.parse(data);

        $("input[name=name]").val(data.name);
        $("input[name=price]").val(data.price);
        $("input[name=start_time]").val(data.start_time);
        $("input[name=link]").val(data.link);
        $("textarea").text(data.description);

        $select.val(null).trigger("change");

        var selected = [];

        $.each(data.categories, function(index, item){
            selected.push(item.id);
        });

        $select.val(selected).trigger("change");
    }

    function setupSelect() {
        return $("#categories").select2({
            palceholder: 'Dodaj zvrsti',
            maximumSelectionLength: 3
        });
    }

    function deleteEvent(id) {
        $.ajax({
            type: 'POST',
            url: 'delete_event.php',
            data: {id: id},
            success: function () {
                $("tr[data-event" + id + "]").remove();
            }
        });
    }
</script>
</body>
</html>
