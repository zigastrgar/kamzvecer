<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Organizer extends Eloquent
{

    protected $table = "organizers";

    protected $fillable
        = [
            'id',
            'name',
            'about',
            'link',
            'address',
            'category_id',
            'facebook_id'
        ];

    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];


    public function events()
    {
        return $this->hasMany(Event::class, "organizer_id", "id");
    }

    public function category()
    {
        return $this->hasOne(Category::class, "id", "category_id");
    }

    public function scopeFacebook($query)
    {
        return $query->where('link', 'like', '%facebook.com%');
    }
}