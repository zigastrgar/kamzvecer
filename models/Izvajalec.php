<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Izvajalec extends Eloquent
{
    protected $table = "izvajalci";

    protected $primaryKey = "id";
}