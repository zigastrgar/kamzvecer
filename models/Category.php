<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Category extends Eloquent
{
    protected $table = "categories";

    protected $fillable = [
        'id',
        'name'
    ];

    public function events()
    {
        return $this->belongsToMany(Event::class);
    }

    public function organizer()
    {
        return $this->hasMany(Organizer::class, "category_id", "id");
    }
}