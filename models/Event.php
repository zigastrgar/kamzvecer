<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Eloquent
{
    use SoftDeletes;

    protected $table = "events";

    protected $fillable
        = [
            'name',
            'name_raw',
            'description',
            'description_raw',
            'location',
            'price',
            'link',
            'organizer_id',
            'category_id',
            'start_time',
            'end_time',
            'published'
        ];

    protected $dates = [ 'start_time', 'end_time' ];

    public function organizer()
    {
        return $this->hasOne(Organizer::class, "id", "organizer_id");
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}