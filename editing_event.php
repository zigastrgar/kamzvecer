<?php

require __DIR__ . '/bootstrap.php';

$id = (int)$_GET['id'];

$_POST['published'] = ($_POST['published'] == 'on') ? 1 : 0;

$event = Event::findOrFail($id)->update($_POST);

$event->categories()->sync($_POST['categories']);

header('Location: events.php');