<?php
require __DIR__ . "/bootstrap.php";

use Carbon\Carbon;

foreach ( Organizer::facebook()->get() as $organizer ) {
    try {
        $response
            = $fb->get('/' . $organizer->facebook_id . "/events?fields=id,name,description,cover,place,start_time,end_time", $_SESSION['fb_access_token']);
    } catch ( Facebook\Exceptions\FacebookResponseException $e ) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch ( Facebook\Exceptions\FacebookSDKException $e ) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $data   = $response->getDecodedBody();
    $events = $data['data'];
    foreach ( $events as $event ) {
        $start_time = Carbon::parse($event['start_time']);
        if ( $start_time->isFuture() ) {
            $event = Event::create([
                'name'            => $event['name'],
                'name_raw'        => $event['name'],
                'description'     => ( strlen($event['description']) > 0 ) ? $event['description'] : "",
                'description_raw' => ( strlen($event['description']) > 0 ) ? $event['description'] : "",
                'location'        => ( !is_null($event['place']) ) ? $event['place']['location']['street'] . ", " . $event['place']['location']['zip'] . " " . $event['place']['location']['city'] : null,
                'start_time'      => $start_time,
                'end_time'        => ( !is_null($event['end_time']) ) ? Carbon::parse($event['end_time']) : null,
                'organizer_id'    => $organizer->id,
                'price'           => getPriceFromDescription($event['description']),
                'link'            => "https://www.facebook.com/events/{$event["id"]}/"
            ]);
            $event->categories()->attach([ ( $organizer->category_id ) ? $organizer->category_id : 12 ]);
        }
    }
}