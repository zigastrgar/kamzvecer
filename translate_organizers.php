<?php

require __DIR__ . '/bootstrap.php';

function getFacebookUsername($link)
{
    $www = "";
    if ( strpos($link, "//www.") !== false ) {
        $www = "www.";
    }
    $remaining = str_replace("https://{$www}facebook.com/", "", $link);

    return explode("/", $remaining)[0];
}

$organizers = Izvajalec::all();

foreach ( $organizers as $organizer ) {
    if ( str_contains($organizer->facebook, "https://www.facebook.com/") && strlen($organizer->facebook) > 0 ) {


        $username = getFacebookUsername($organizer->facebook);
        if ( str_contains($username, "-") ) {
            $username = end(explode("-", $username));
        }

        try {
            $response = $fb->get('/' . $username . '?fields=id,name,about,location,link', $_SESSION['fb_access_token']);
        } catch ( Facebook\Exceptions\FacebookResponseException $e ) {
            if ( $e->getCode() == 803 ) {
                $username = explode("?", $username)[0];
                $response = $fb->get('search?type=user&q=' . $username, $_SESSION['fb_access_token']);
                $data   = $response->getDecodedBody();
                $userid = $data['data'][0]['id'];
                $response
                        = $fb->get('/' . $userid . '?fields=id,name,about,location,link', $_SESSION['fb_access_token']);
            } else {
                echo $organizer->facebook;
                echo 'Graph2 returned an error: ' . $e->getMessage();
                exit;
            }
        } catch ( Facebook\Exceptions\FacebookSDKException $e ) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();

        Organizer::create([
            'name'        => ( strlen($organizer->name) > 0 ) ? $organizer->name : $user['name'],
            'about'       => ( strlen($user['about']) > 0 ) ? $user['about'] : "",
            'link'        => $user['link'],
            'address'     => $user['location']['street'] . ", " . $user['location']['zip'] . " " . $user['location']['city'],
            'facebook_id' => $user['id'],
            'category_id' => ( strlen($organizer->code) > 0 ) ? str_replace("zvrst", "", $organizer->code) : null
        ]);
    } else {
        Organizer::create([
            'name'        => $organizer->name,
            'about'       => "",
            "address"     => "",
            "link"        => $organizer->facebook,
            'category_id' => ( strlen($organizer->code) > 0 ) ? str_replace("zvrst", "", $organizer->code) : null
        ]);
    }
}