<?php
require __DIR__ . "/bootstrap.php";
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vsi dogodki</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <h2>Verzija .01
            <small><span class="label label-warning">BETA</span></small>
        </h2>
        <p>Zenkrat se podatke ne obdelujejo, kot dobimo iz Facebooka tako se prikaže tu</p>
        <p><strong>Opis je zaenkrt skrit zaradi količine teksta!</strong></p>
        <table class="table table-bordered table-responsive">
            <tr>
                <th>Ime</th>
                <th>Povezava</th>
                <th>Kategorija</th>
            </tr>
            <?php foreach ( Organizer::all() as $organizer ) { ?>
                <tr>
                    <td><?= $organizer->name ?></td>
                    <td><a href="<?= $organizer->link ?>" target="_blank"><?= $organizer->name ?></a></td>
                    <td><?= $organizer->category->name; ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
</body>
</html>
