<?php

function smartName($name)
{

}

function getPriceFromDescription($description)
{
    foreach ( explode(PHP_EOL, mb_strtolower($description)) as $line ) {
        if ( str_contains($line, [
                "ni vstopnine",
                "brez vstopnine",
                "vstopnine ni",
                "vstop brezplačen",
                "prost vstop",
                "je brezplačen"
            ]) !== false
        ) {
            return 0;
        }

        if ( preg_match("/(<?[0-9]+) ?€/", $line, $results) == 1 ) {
            return (int)$results[1];
        }
    }

    return null;
}

function smartDescription($description)
{

}