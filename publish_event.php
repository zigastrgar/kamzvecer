<?php
require_once __DIR__ . '/bootstrap.php';

$id = (int)$_GET['id'];

$event = Event::findOrFail($id);

if ( $event->published == 1 ) {
    $event->published = 0;
} else {
    $event->published = 1;
}

$event->save();

header('Location: events.php');
